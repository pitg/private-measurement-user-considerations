# User considerations for private measurement

### Status of this document

Outline of user considerations for private measurement proposals, which may be helpful in evaluating proposals at IETF and W3C for measurement, attribution and advertising-related work.

Editor: [Nick Doty](https://npdoty.name), <ndoty@cdt.org>

## Definitions

This document defines the "user" of a private measurement system as a participant in the system.  That is, someone whose data is being measured and who has a privacy interest in ensuring that they or their data are not identified in the resulting measurements.

Other parties (beyond *users* of these systems) might also be involved, including:

- *designers*, who specify what sorts of aggregate data is intended to be produced, what data will be gathered, and how it will be processed.

- *operators*, who run some of the infrastructure of the privacy-preserving measurement apparatus, without necessarily getting access to either the reported data or the aggregate results.

- *consumers*, who get access to the aggregate measurement results.

## Comprehensibility

That users can comprehend how the system works to some extent may be a prerequisite for informed consent and may be useful for encouraging users to participate in the system (opting-in to it or not opting-out of it).

Understanding the cryptography of a system is a laudable goal, but not necessary for comprehensibility of the implications of a system or the choice to participate.

Users can also benefit from a system being generally comprehensible to parties that they trust (researchers, local regulators, consumer advocacy organization they know, etc.).

What specifically do users (or any party acting as their agent) need to be able to comprehend?  The following sections describe specific details that need to be comprehensible.

## Trust Model

Many private measurement systems rely on trusting in multiple parties, that those parties will not collude or that those parties are non-malicious in the systems they provide. 

Whom do users need to trust to confirm their privacy in the system? How are those parties selected? How can the user identify the parties involved?  How can the user know if the parties involved change?

When parties collude, how bad is the outcome for the user? If the parties collude, will the user be able to tell?  How many parties need to collude for the outcomes to change?

Can the user (or any party acting as their agent) deliberately exclude some parties from participation (e.g., when one of the parties is suspected to be untrustworthy)?  What happens to the measurement results in this case?  What happens to the user's expected privacy guarantees?

## Transparency

What measurement systems is the user engaged in? What kind of measurements, breakdowns or analyses will be available and to whom?

## Control

For ethical construction of a measurement, users must be able to disengage from participation in the system.

Can disengagement happen after participation, or is opt-out only prospective, not retrospective?

## Participation

Users may volunteer to participate in measurements or studies, particularly when the purpose is clear and aligned with their interests.

## Privacy

_(Although most of the above sections could be considered part of privacy as well, this could be a place to point to privacy analyses and considerations about information that may be disclosed.)_

What information is collected about users and with whom is it shared? What is the risk that a party will learn something about the user's online or offline behavior?

How is privacy measured?  For example, Differential Privacy systems are often defined or tuned in terms of ε (or ε and δ), but these tuning parameters may or may not be visible to the participant.

## Performance

By engaging in private measurement systems, what performance costs are users likely to pay? Could performance simultaneously be improved compared to status quo measurement systems?

Performance costs might include network traffic, computation, local storage, additional latency for other traffic, etc.

## Availability

Privacy-preserving measurement systems should not be in the critical path of the user's experience. Telemetry systems are usually not critical from the point of view of the user and as such should fail gracefully. Care should especially be taken to make sure that when the system fails, the user's privacy should not be impacted by degrading to a less-secure telemetry system. 

## Sustainability

What are the sustainability implications (including carbon emissions and waste) of a private measurement system? Could full scope emissions be lower compared to status quo systems? How will energy usage and emissions be measured and reduced over time?

## Cost

The deployment of telemetry systems comes at some cost to the operator, which may in turn be passed on to (some of) the users.  Are there other options that the operator could pursue that could achieve comparable goals with comparable accuracy, with similar or lesser costs?

---

## Acknowledgements

Thanks for reviews and contributions:

* pitg
* Shivan Sahib
* dkg
* ...

## See also

* [patcg security and privacy threat model](https://github.com/patcg/docs-and-reports/blob/main/threat-model/readme.md)
* [private ad technologies principles](https://patcg.github.io/docs-and-reports/principles/)
